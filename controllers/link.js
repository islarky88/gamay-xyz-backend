const cryptoRandomString = require('crypto-random-string');
const Sequelize = require('sequelize');

const Link = require('../models/link');

exports.redirect = async (req, res, next) => {
	const slug = req.params.slug;

	try {
		// finds for avaialble slug
		const response = await Link.findOne({ where: { slug: slug } });

		//checks if we found the lug
		if (response) {
			// redirect to url that matched the slug
			res.redirect(response.url);
		} else {
			// send an error message
			res.status(404).send('Sorry, we cannot find that!');
		}
	} catch (error) {
		console.log(error);
	}
};

exports.shorten = async (req, res, next) => {
	const urls = req.body.urls;

	let results = [];
	let slug = '';
	let currentSlug = '';
	try {
		if (urls.length == 0) {
			const error = new Error('no urls found');
			throw error;
		}

		for (i = 0; i < urls.length; i++) {
			//generates a random slug
			slug = cryptoRandomString(5);

			//checks if slug is already taken
			currentSlug = await Link.findOne({ where: { slug: slug } });

			// if slug not taken yet, prepare to save to DB
			if (currentSlug === null) {
				results.push({ slug: slug, url: urls[i].url });
			}
		}

		// gives error if no links found and converted
		if (results.length === 0) {
			const error = new Error('no links converted');
			throw error;
		}

		// saves all the submitted urls to database with individual slugs
		let result = await Link.bulkCreate(results, { individualHooks: true });

		// send suuccess results
		res.status(201).json({
			message: 'success',
			data: result
		});
	} catch (err) {
		if (!err.statusCode) {
			err.statusCode = 500;
		}
		next(err);
	}
};
