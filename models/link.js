const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const Link = sequelize.define('links', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		allowNull: false,
		primaryKey: true
	},
	slug: {
		type: Sequelize.STRING,
		allowNull: false
	},
	url: {
		type: Sequelize.STRING,
		allowNull: false
	}
});

module.exports = Link;
