const Sequelize = require('sequelize')

const sequelize = require('../util/database')

const User = sequelize.define('users', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  username: { type: Sequelize.STRING },
  password: { type: Sequelize.STRING, allowNull: false },
  salt: { type: Sequelize.STRING, allowNull: false },
  email: { type: Sequelize.STRING, allowNull: false },
  // emailConfirm: { type: Sequelize.STRING },
  // name: { type: Sequelize.STRING },
  // nickname: { type: Sequelize.STRING },
  // role: { type: Sequelize.INTEGER },
  // token: { type: Sequelize.STRING },
  // loggedIn: { type: Sequelize.INTEGER },
  // online: { type: Sequelize.INTEGER },
  // lastActivity: { type: Sequelize.STRING },
  // pageviews: { type: Sequelize.INTEGER },
  // onlineTime: { type: Sequelize.INTEGER },
  // linkCount: { type: Sequelize.INTEGER },
  // editCount: { type: Sequelize.INTEGER },
  // reportCount: { type: Sequelize.INTEGER },
  // grabCount: { type: Sequelize.INTEGER }
})

module.exports = User
