const express = require('express');

const linkController = require('../controllers/link');

const router = express.Router();

router.post('/api/link/shorten', linkController.shorten);

router.get('/:slug([A-Za-z0-9]+)', linkController.redirect);

module.exports = router;
